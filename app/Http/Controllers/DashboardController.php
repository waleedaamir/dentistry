<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Auth;

use Laravel\Socialite\Facades\Socialite;

use Illuminate\Support\Facades\DB;
use Psy\Readline\Hoa\Console;
use Illuminate\Support\Facades\Storage;
use File;
use URL;

use Jenssegers\Agent\Agent;

$agent = new Agent();

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}




class DashboardController extends Controller
{

//Patch for Force User Logged
public function __construct() {
    // Code called for each new Person we create
            //Patch for Force Loggedin
            $_SESSION['email'] = "raghibdev2001@gmail.com";
            $_SESSION['signin1'] = 'yes';
            $_SESSION['username'] = "Raghib";
            $_SESSION['privilege'] = "3";
}

    public function mailNow($message, $fromEmail, $toEmail, $subject, $userName) {

        $dianKey = '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';

        $email = new \SendGrid\Mail\Mail();
        $email->setfrom($fromEmail, $userName);
        $email->setSubject($subject);
        $email->addto($toEmail, 'DIAN');
        $email->addContent("text/plain", $message);

        $sendgrid = new \SendGrid($dianKey);

        try {
            $response = $sendgrid->send($email);

            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";

        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    function shopifyLogoUrl(){
        if(isset($_SESSION['email'])){
            return redirect('dashboard');
        }else{
            return redirect('home');
        }
    }

    function contact(){
        $hideSignin = FALSE;
            if(isset($_SESSION['email'])){
                $hideSignin = TRUE;
            }
        return view('contact',['hideSignin'=>$hideSignin]);
    }

    function home()
    {
        $hideSignin = FALSE;
        if(isset($_SESSION['email'])){
            $hideSignin = TRUE;
        }
        return view('home',['hideSignin'=>$hideSignin]);
    }

    function welcome()
    {
        $hideSignin = FALSE;
        if(isset($_SESSION['email'])){
            $hideSignin = TRUE;
        }
        return view('welcome',['hideSignin'=>$hideSignin]);
    }

    function aboutUs()
    {
        $hideSignin = FALSE;
        if(isset($_SESSION['email'])){
            $hideSignin = TRUE;
        }
        return view('aboutUs',['hideSignin'=>$hideSignin]);
    }

    function packageInfo(){
        return 'packageInfo';
    }

    function uploadInitialProfileData(Request $request)
    {
        if ($request->firstName && $request->lastName) {

            $firstName = $request->firstName;
            $lastName = $request->lastName;

            $email = $_SESSION['email'];

            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user != '') {
                if ($request->hasFile('profilePic')) {
                    $profilePic =  $request->file('profilePic');

                    $fileExtension = $profilePic->getClientOriginalExtension();
                    $fileName = $profilePic->getClientOriginalName();

                    $profilePicName = $fileName;

                    Storage::put('profile_pics/'.$fileName, (string) file_get_contents($profilePic), 'public');
                } else {
                    $profilePicName = 'user.png';
                }
                if ($request->hasFile('initialStatement')) {
                    $statement =  $request->file('initialStatement');
                    $statementFileExtension = $statement->getClientOriginalExtension();
                    $statementFileName = $statement->getClientOriginalName();

                    $statementName = $statementFileName;

                    Storage::put('statements/'.$statementFileName, (string) file_get_contents($statement), 'public');
                } else {
                    $statementName = 'file.png';
                }
                $updateUser = DB::table('users')->where('email', $email)->update([
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'profilePic' => $profilePicName,
                    'statement' => $statementName,
                    'statementOne' => 'file.png',
                    'statementTwo' => 'file.png',
                    'statementThree' => 'file.png',
                ]);
                return redirect('dashboard');
            }
        } else {
            return redirect();
        }
    }

    function updateUserProfile(Request $request)
    {
            $email = $_SESSION['email'];

            $user = DB::table('users')->where('email', $email)->get()->first();

            if($request->firstName){
                $firstName = $request->firstName;
            }else{
                $firstName = $user->firstName;
            }

            if($request->firstName){
                $lastName = $request->lastName;
            }else{
                $lastName = $user->lastName;
            }

            if ($user != '') {
                if ($request->hasFile('editProfilePic')) {
                    $profilePic =  $request->file('editProfilePic');

                    $fileExtension = $profilePic->getClientOriginalExtension();
                    $fileName = $profilePic->getClientOriginalName();

                    $profilePicName = $fileName;

                    Storage::put('profile_pics/'.$fileName, (string) file_get_contents($profilePic), 'public');

                } else {
                    $profilePicName = $user->profilePic;
                }
                if ($request->hasFile('editStatement')) {
                    $statement =  $request->file('editStatement');
                    $statementFileExtension = $statement->getClientOriginalExtension();
                    $statementFileName = $statement->getClientOriginalName();

                    $statementName = $statementFileName;

                    Storage::put('statements/'.$statementFileName, (string) file_get_contents($statement), 'public');
                } else {
                    $statementName = $user->statement;
                }

                if ($request->hasFile('editStatementOne')) {
                    $statementOne =  $request->file('editStatementOne');
                    $statementFileExtensionOne = $statementOne->getClientOriginalExtension();
                    $statementFileNameOne = $statementOne->getClientOriginalName();

                    $statementNameOne = $statementFileNameOne;

                    Storage::put('statements/'.$statementFileNameOne, (string) file_get_contents($statementOne), 'public');
                } else {
                    $statementNameOne = $user->statementOne;
                }
                if ($request->hasFile('editStatementTwo')) {
                    $statementTwo =  $request->file('editStatementTwo');
                    $statementFileExtensionTwo = $statementTwo->getClientOriginalExtension();
                    $statementFileNameTwo = $statementTwo->getClientOriginalName();

                    $statementNameTwo = $statementFileNameTwo;

                    echo $statementNameTwo;

                    Storage::put('statements/'.$statementFileNameTwo, (string) file_get_contents($statementTwo), 'public');
                } else {
                    $statementNameTwo = $user->statementTwo;
                }
                if ($request->hasFile('editStatementThree')) {
                    $statementThree =  $request->file('editStatementThree');
                    $statementFileExtensionThree = $statementThree->getClientOriginalExtension();
                    $statementFileNameThree = $statementThree->getClientOriginalName();

                    $statementNameThree = $statementFileNameThree;

                    Storage::put('statements/'.$statementFileNameThree, (string) file_get_contents($statementThree), 'public');
                } else {
                    $statementNameThree = $user->statementThree;
                }
                $updateUser = DB::table('users')->where('email', $email)->update([
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'profilePic' => $profilePicName,
                    'statement' => $statementName,
                    'statementOne' => $statementNameOne,
                    'statementTwo' => $statementNameTwo,
                    'statementThree' => $statementNameThree
                ]);

                $user = DB::table('users')->where('email', $email)->get()->first();

            //    return $user;
             return redirect('dashboard');
            }
    }

    function submitSubscribe(Request $request)
    {
        if ($request->has('name') && $request->has('email')) {

            $name = $request->name;
            $email = $request->email;
            $mobileNumber = $request->mobileNumber;

            // Update privlege level of user here in database

            $subscriber = DB::table('subscribers')->where('name', $name)->where('email', $email)->get()->first();

            if ($subscriber == '') {
                $newSubscriber = DB::table('subscribers')->insert([
                    'name' => $name,
                    'email' => $email,
                    'phoneNumber' => $mobileNumber
                ]);

                $response = 'Thanks for Subscribing...';

                return view('subscribe', ['response' => $response]);
            } else {
                $response = 'This email address is alreday registered...';

                return view('subscribe', ['response' => $response]);
            }
        } else {
            return 'FAIL';
        }
    }

    function faq()
    {
        $hideSignin = FALSE;
            if(isset($_SESSION['email'])){
                $hideSignin = TRUE;
            }
        return view('faq',['hideSignin'=>$hideSignin]);
    }


    function dashboard()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];

            $user = DB::table('users')->where('email', $email)->get()->first();

            if($user->status == 'paid'){

                if($user->firstName == '-' && $user->lastName == '-'){
                    return redirect('welcome');
                }else{

                    $privilege = $_SESSION['privilege'];
                    if ($user->status == 'paid' && $privilege >= 1) {

                        $reels = DB::table('reels')->get();
                        $hashtags = DB::table('hashtags')->where('nameOfContentSection','reels')->get();

                        return view('dashboard', ['user'=>$user, 'reels' => $reels, 'hashtags' => $hashtags,'activeMenu'=>'dashboard']);
                    } else {

                        $reels = DB::table('reels')->limit(2)->get();
                        $hashtags = DB::table('hashtags')->where('nameOfContentSection','reels')->get();

                        return view('dashboard', ['user'=>$user, 'reels' => $reels, 'hashtags' => $hashtags,'activeMenu'=>'dashboard']);
                    }
                }
            }else{
                return redirect('subscription-plans');
            }
        } else {
            return redirect('signin');
        }
    }

    function privacyPolicy()
    {
        $hideSignin = FALSE;
        if(isset($_SESSION['email'])){
            $hideSignin = TRUE;
        }
        return view('privacyPolicy',['hideSignin'=>$hideSignin]);
    }

    function termsAndConditions(){
        $hideSignin = FALSE;
        if(isset($_SESSION['email'])){
            $hideSignin = TRUE;
        }
        return view('termsAndConditions',['hideSignin'=>$hideSignin]);

    }

    function hashtagFilter2(Request $request)
    {
        return 'done';
    }

    function businessFinanceHashtagFilter(){

    }

    function hashtagFilter(Request $request)
    {
        if ($request->has('contentType') && $request->has('nameOfHashtag')) {

            $contentType = $request->contentType;
            $hashtagValue = $request->nameOfHashtag;
            $activeMenu = $request->activeMenu;

            $hashtags = DB::table('hashtags')->where('nameOfContentSection',$contentType)->get()->all();

            $hashtagId = '';

            for ($i = 0; $i < count($hashtags); $i++) {
                if ($hashtags[$i]->nameOfHashtag == $hashtagValue) {
                    $hashtagId = $hashtags[$i]->id;
                }
            }

            $finalReels = DB::table($contentType)->where('hashtagId', $hashtagId)->get()->all();

            $email = $_SESSION['email'];

            $user = DB::table('users')->where('email', $email)->get()->first();

            $prvUrl = basename(URL::previous());

            //return 'success';

          // return $hashtags;

            $privilege = $_SESSION['privilege'];

            if ($hashtagValue == 'Webinar' && $privilege == 0) {
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'podcasts','activeMenu'=>'podcast','message'=>$message]);
            }

             return view('hashtagFilter', ['user'=>$user, 'finalReels' => $finalReels, 'hashtags' => $hashtags, 'activeMenu' => $activeMenu,'selectedHashtagValue'=>$hashtagValue]);
        } else {
            return 'pass all parameters';
        }
    }

    function podcast()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();
            $hashtags = DB::table('hashtags')->where('nameOfContentSection','podcasts')->get();
            $firstHashtag = DB::table('hashtags')->where('nameOfContentSection','podcasts')->get()->first();
            $podcasts = DB::table('podcasts')->where('hashtagId',$firstHashtag->id)->get()->reverse();
            $hashtagValue = $firstHashtag->nameOfHashtag;

            if ($user->status == 'paid' && $_SESSION['privilege'] >= 0 && $hashtagValue == 'Podcasts') {

                // $podcasts = DB::table('podcasts')->get()->all();

                // return view('podcast', ['user'=>$user, 'podcasts' => $podcasts ,'hashtags' => $hashtags, 'contentType' => 'podcasts','activeMenu'=>'podcast']);
                return view('hashtagFilter', ['user'=>$user, 'finalReels' => $podcasts, 'hashtags' => $hashtags, 'activeMenu' => 'podcast','selectedHashtagValue'=>$hashtagValue]);

            }else if ($hashtagValue == 'Webinar') {
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'podcasts','activeMenu'=>'podcast','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function courses()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege'] >=2 ) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','courses')->get();
                return view('courses', ['user'=>$user, 'hashtags' => $hashtags,'activeMenu'=>'courses','contentType'=>'courses']);
            }else{

                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'courses','activeMenu'=>'courses','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function courses1()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege'] >=2 ) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','courses')->get();
                return view('courses', ['user'=>$user, 'hashtags' => $hashtags,'activeMenu'=>'courses','contentType'=>'courses']);
            }else{

                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'courses','activeMenu'=>'courses','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function healthAndWellbeing()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege']>=2 ) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','healthAndWellBeing')->get();
                $firstHashtag = DB::table('hashtags')->where('nameOfContentSection','healthAndWellBeing')->get()->first();
                $healths = DB::table('healthAndWellbeing')->where('hashtagId',$firstHashtag->id)->get()->all();
                return view('healthAndWellbeing', ['user'=>$user, 'hashtags' => $hashtags,'healths' => $healths,'activeMenu' => 'healthAndWellbeing','contentType'=>'healthAndWellbeing']);
            }else{

                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'healthAndWellbeings','activeMenu'=>'healthAndWellbeing','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function buildYourBusiness()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege']>=2) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','businessAndFinances')->get();
                $financeHashtag = DB::table('hashtags')->where('nameOfContentSection','businessAndFinances')->where('nameOfHashtag','Finance - Shorts')->get()->first();
                $reels = DB::table('businessAndFinances')->where('hashtagId',$financeHashtag->id)->get()->all();
                $businessHashtag = DB::table('hashtags')->where('nameOfContentSection','businessAndFinances')->where('nameOfHashtag','Business')->get()->first();
                $businessReels = DB::table('businessAndFinances')->where('hashtagId',$businessHashtag->id)->get()->all();

                return view('buildYourBusiness', ['user'=>$user,'reels'=>$reels,'businessReels'=>$businessReels, 'hashtags' => $hashtags,'activeMenu'=>'buildYourBusiness','contentType'=>'businessAndFinances']);
            }else{

                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'businessAndFinances','activeMenu'=>'buildYourBusiness','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function downloads()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($_SESSION['privilege']>=2) {
                $hashtags = DB::table('hashtags')->where('nameOfContentSection','downloads')->get();

                $path    = './downloads/';
                $files = scandir($path);
                $fileNames = [];
                for($i=0; $i< count($files); $i++){
                    if($files[$i] != 'thumbnails'){
                        //array_push($fileNames,ucwords(strtolower($files[$i])));
                        array_push($fileNames,$files[$i]);
                    }
                }
                $fileNames = array_diff($fileNames, array('.', '..'));
                sort($fileNames);

                // $files = array_diff(scandir($path), array('.', '..'));

                $pathThumbnails    = './downloads/thumbnails';
                $thumbnailFiles = scandir($pathThumbnails);
                $thumbnailFiles = array_diff(scandir($pathThumbnails), array('.', '..'));

                //return $fileNames;
                return view('downloads', ['user'=>$user,'files' => $fileNames, 'hashtags' => $hashtags, 'thumbnailFiles' => $thumbnailFiles, 'activeMenu'=>'downloads','contentType'=>'downloads']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'downloads','activeMenu'=>'downloads','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function assist()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            $privilege = $user->privilege;

            if ($user->status == 'paid' && $privilege>=2) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','assists')->get();

                return view('assist', ['user'=>$user,'hashtags' => $hashtags,'contentType' => 'assists','activeMenu'=>'assist']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'assists','activeMenu'=>'assist','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function assistVideos(){

        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege']>=2) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','assists')->get();

                $assists = DB::table('assists')->get()->all();

                return view('assistVideos', ['user'=>$user,'hashtags' => $hashtags,'assists'=>$assists,'contentType'=>'assist','activeMenu'=>'assist']);
            }else{

                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'assist','activeMenu'=>'assist','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function student()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();
            $firstHashtag = DB::table('hashtags')->where('nameOfContentSection','students')->get()->first();
            $students = DB::table('students')->where('hashtagId',$firstHashtag->id)->get()->reverse();
            $hashtagValue = $firstHashtag->nameOfHashtag;

            if ($user->status == 'paid' && $_SESSION['privilege']>=1) {

                $path    = './student/';
                $files = scandir($path);
                $files = array_diff(scandir($path), array('.', '..'));

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','students')->get();

                $agent = new Agent();
                return view('hashtagFilter', ['user'=>$user, 'finalReels' => $students, 'hashtags' => $hashtags, 'activeMenu' => 'student','selectedHashtagValue'=>$hashtagValue]);

                // return view('student', ['user'=>$user, 'hashtags' => $hashtags, 'files' => $files,'activeMenu'=>'student','contentType'=>'students']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'students','activeMenu'=>'student','message'=>$message]);
            }
        } else {
            return redirect('signin');
        }
    }

    function refer()
    {
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid') {
                $hashtags = DB::table('hashtags')->where('nameOfContentSection','refer')->get();
                $refers = DB::table('refers')->get()->all();
                return view('refer', ['user'=>$user,'refers'=>$refers, 'hashtags' => $hashtags, 'activeMenu' => 'refer','contentType'=>'refers']);
            }else{
                return redirect('stripe');
            }
        } else {
            return view('signin');
        }
    }

    function guidelines()
    {
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid'  && $_SESSION['privilege']>=2) {
                $hashtags = DB::table('hashtags')->where('nameOfContentSection','guidelines')->get();

                $fileNames = DB::table('guidelines')->get()->all();

                return view('guidelines', ['user'=>$user, 'fileNames' => $fileNames, 'hashtags' => $hashtags,'activeMenu' => 'guidelines','contentType'=>'guidelines']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'guidelines','activeMenu'=>'guidelines','message'=>$message]);
            }
        } else {
            return view('signin');
        }
    }

    function pubMed(){
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid'  && $_SESSION['privilege']>=0) {
                return view('pubmed', ['user'=>$user, 'activeMenu' => 'pubMed','contentType'=>'pubMed']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'pubMed','activeMenu'=>'pubMed','message'=>$message]);
            }
        } else {
            return view('signin');
        }
    }

    function submitPubMed(Request $request){

        $searchWord = $request->searchWord;

        $url = "https://pubmed.ncbi.nlm.nih.gov/?term=".$searchWord;

        return redirect()->away($url);
    }

    function copd(){
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid'  && $_SESSION['privilege']>=0) {
                return view('copd', ['user'=>$user, 'activeMenu' => 'copd','contentType'=>'copd']);
            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'copd','activeMenu'=>'copd','message'=>$message]);
            }
        } else {
            return view('signin');
        }
    }

    function submitcopd(Request $request){

        $email = $_SESSION['email'];
        $user = DB::table('users')->where('email',$email)->get()->first();
        $userName = $user->name;
        $gdcNumber = $request->gdcNumber;

        // Get an array of checked checkboxes
        $checkedCheckboxes = $request->input('checkboxes', []);

        // $checkedCheckboxes will contain an array of checked checkboxes.

        $value = '';
        // You can loop through the checked checkboxes and do whatever you need to do.
        for($i=0; $i<count($checkedCheckboxes);$i++) {
            if($value != ''){
                $value = $value . ", " . $checkedCheckboxes[$i];
            }else{
                $value = $checkedCheckboxes[$i];
            }

        }

        $message =
        "Hello \n\nEmail: " . $email .
        "\n\nUsername: " . $userName .
        "\n\nGDC Number: " . $gdcNumber .
        "\n\nThe user has completed all the tasks and marked as complete in the CPD form".
        "\n\n The list of tasks marked are " . "\n\n" . $value;

        $fromEmail = 'noreply@dentistryinanutshell.com';
        $toEmail = 'dianclubltd@gmail.com';
        $subject = "Received a response from CPD!";

        $this->mailnow($message, $fromEmail, $toEmail, $subject, "DIAN");

        return redirect("profile")->with('success', 'CPD submitted successfully');
    }

    function setupProfile()
    {
        if(isset($_SESSION['email'])){
            return view('setupProfile');
        }else{
            return redirect('signin');
        }
    }

    function profile()
    {
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email',$email)->get()->first();
            if($user->status == 'paid'){

                if($user->firstName == '-' && $user->lastName == '-'){
                    return redirect('welcome');
                }else{


                    $subscription = DB::table('subscriptions')->where('userEmail', $email)->get()->first();
                    $startDate = $subscription->startDate;
                    $startDate =
                        gmdate("d-m-Y", $startDate);

                    // echo $startDate;
                    $endDate =
                        date('d-m-Y', strtotime($startDate . ' + 30 days'));

                    $now = time();
                    $noOfDays = strtotime($endDate) - $now;
                    $noOfDays =
                        round($noOfDays / (60 * 60 * 24));

                    // echo '-----';
                    // echo round($noOfDays / (60 * 60 * 24));
                    // echo '-----';

                    $startDateExplode = explode('-',$startDate);

                    $plan = DB::table('plans')->where('id',$subscription->planId)->get()->first();

                    if($plan->name == 'starter' || $plan->name == 'student' || $plan->name == 'premium'){
                        $autoRenewelDate = $startDateExplode[0] .'th of every month';
                    }else{
                        $monthName = date("F", mktime(0, 0, 0, $startDateExplode[1], 1));
                        $autoRenewelDate = $startDateExplode[0] .'th of '. $monthName . ' every year';
                    }

                    $message = '';
                    $checkList = DB::table('checkLists')->where('userEmail',$email)->get()->first();

                    if($checkList !=''){
                        $checkListStatus = $checkList -> checkList;
                    }else{
                        $checkListStatus = 0;
                    }

                    $cpdLists = [];
                    $reels = DB::table('reels')->get('name')->all();
                    $podcasts = DB::table('podcasts')->get('name')->all();

                    for($i=0;$i<count($reels);$i++){
                        array_push($cpdLists,$reels[$i]);
                    }

                    for($i=0;$i<count($podcasts);$i++){
                        array_push($cpdLists,$podcasts[$i]);
                    }

                    $status = "active";

                    if($user->status == "cancelled"){
                        $status = "cancelled";
                    }

                    if($user->privilege == 0){
                        $subscriptionPlan = 'Starter';
                    }else if($user->privilege == 1){
                        $subscriptionPlan = 'Student';
                    }else if($user->privilege == 2){
                        $subscriptionPlan = 'Premium';
                    }else{
                        $subscriptionPlan = 'ADMIN';
                    }

                    return view('profile', ['autoRenewelDate'=>$autoRenewelDate,'subscriptionPlan'=>$subscriptionPlan, 'status'=>$status,'cpdLists'=>$cpdLists, 'checkListStatus'=>$checkListStatus, 'message'=>$message, 'user'=>$user, 'startDate' => $startDate, 'endDate' => $endDate, 'noOfDays' => $noOfDays]);
                }
            }else{
                return redirect('subscription-plans');
            }
        } else {
            return view('signin');
        }
    }



    function singleBlog(Request $request)
    {
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($_SESSION['privilege']>=0) {

                if ($request->has('blogId')) {

                    $blogId = $request->blogId;
                    $blogs = DB::table('blogs')->where('id', $blogId)->get()->first();
                    $hashtags = DB::table('hashtags')->where('nameOfContentSection','blogs')->get();
                    // return $blogs;
                    return view('singleBlog', ['user'=>$user, 'blogs' => $blogs, 'hashtags' => $hashtags,'activeMenu'=>'blogs', 'contentType'=>'blogs']);
                }
            }
        } else {
            return view('signin');
        }
    }

    function blogs()
    {
        $blogs = DB::table('blogs')->get()->reverse();

        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($_SESSION['privilege']>=0) {

                $hashtags = DB::table('hashtags')->where('nameOfContentSection','blogs')->get();
                return $hashtags;
                // return view('blogs', ['user'=>$user, 'blogs' => $blogs, 'hashtags' => $hashtags, 'activeMenu'=>'blogs', 'contentType'=>'blogs']);
            }
        } else {
            return view('signin');
        }
    }

    function allBlogs(){
        $firstHashtag = DB::table('hashtags')->where('nameOfContentSection','blogs')->get()->first();
        $blogs = DB::table('blogs')->where('hashtagId',$firstHashtag->id)->get()->all();
        $hashtagValue = $firstHashtag->nameOfHashtag;

        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();
            if ($_SESSION['privilege']>=0) {
                $hashtags = DB::table('hashtags')->where('nameOfContentSection','blogs')->get();
                return view('hashtagFilter', ['user'=>$user, 'finalReels' => $blogs, 'hashtags' => $hashtags, 'activeMenu' => 'blogs','selectedHashtagValue'=>$hashtagValue]);
                // return view('blogs', ['user'=>$user, 'blogs' => $blogs, 'hashtags' => $hashtags, 'activeMenu'=>'blogs', 'contentType'=>'blogs']);

            }
        } else {
            return view('signin');
        }
    }

    function workFlows(){
        if (isset($_SESSION['email'])) {

            $email = $_SESSION['email'];
            $user = DB::table('users')->where('email', $email)->get()->first();

            if ($user->status == 'paid' && $_SESSION['privilege']>=1) {
                $hashtags = DB::table('hashtags')->where('nameOfContentSection','workFlows')->get();

                $agent = new Agent();

                if($agent->isMobile()){
                    $firstHashtag = DB::table('hashtags')->where('nameOfContentSection','workFlows')->get()->first();
                    $workFlows = DB::table('workFlows')->where('hashtagId',$firstHashtag->id)->get()->reverse();
                    $hashtagValue = $firstHashtag->nameOfHashtag;

                    return view('hashtagFilter', ['user'=>$user, 'finalReels' => $workFlows, 'hashtags' => $hashtags, 'activeMenu' => 'workFlows','selectedHashtagValue'=>$hashtagValue]);
                    // return view('workFlows', ['user'=>$user, 'hashtags' => $hashtags,'activeMenu' => 'workFlows','contentType'=>'workFlows']);
                }else{
                    return view('noDesktopAccess', ['user'=>$user,'activeMenu'=>'workFlows','contentType'=>'workFlows']);
                }


            }else{
                $message = 'Upgrade your subcription to access this page';
                return view('noSubscriptionAccess', ['user'=>$user, 'contentType' => 'workFlows','activeMenu'=>'workFlows','message'=>$message]);
            }
        } else {
            return view('signin');
        }
    }

}
