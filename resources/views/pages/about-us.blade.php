@extends ('layouts.layout_2')

@section('head')
    <title>Home &#8211; Dian</title>
@endsection

<style>
    .box {
        background-color: #102335;
        /* width: 300px; */
        /* border: 15px solid green; */
        padding: 40px;
        margin-bottom: 20px;
        height: 300px;
    }

    .w-6 {
        width: 6rem !important;
    }

    .notes {
        font-size: 30px;
        font-weight: 300;
    }


</style>



@section('content')
    <div class="content-body">

        {{-- @include('pages.subheader') --}}


        <div class="container-fluid">


            <div class="row">

                <div class="col-md-6">

                    <p class="introducin">Introducing</p>
                    <p class="your_perso">Your Personalised Platform for
                        Dentists</p>
                    <p class="step_into_">Step into DIAN Club, the platform crafted exclusively with your needs in mind.
                        We've woven your aspirations and desires into every aspect.</p>
                    <p class="step_into_">Unveil a world of tailored resources—immersive videos, captivating podcasts, and
                        insightful blogs—that resonate with your journey in dentistry. Our AI-driven tools are finely tuned
                        to your workflow, easing patient interactions and note-taking.</p>
                    <p class="step_into_">The courses and webinars? They're shaped by your thirst for knowledge and growth,
                        whether you're a seasoned expert or an aspiring student. And because we care about your well-being,
                        find a dedicated space offering valuable insights to balance your career and life.</p>
                    <p class="step_into_" style="color: white !important">Join the club and become a part of our community!
                    </p>
                    <button type="button" class="btn3  anek-telugu">Join Us Now</button>
                </div>
                <div class="col-md-6">

                    <img class="" src="{{ asset('images/assist/about.png') }}">
                    <div class="py-3"></div>
                    <p class="step_into_"> For practice owners like you, our support spans beyond dentistry. Count on us for
                        informed decisions,
                        enriched by financial insights and business tools. Your voice echoes in our interactive forum—a
                        community that listens, shares, and uplifts.</p>

                    <p class="step_into_"> This is your moment—DIAN Club is your masterpiece, reflecting your journey,
                        desires, and dreams. Elevate your dental career, exclusively with DIAN Club.</p>


                </div>
            </div>

            <div class="row">
                <p class="your_perso">Meet The Team</p>
                <div class="col-md-6">
                    <div class="box">

                        <div class="row">
                            <div class="col-md-5">
                                <img class="" src="{{ asset('images/assist/profile1.png') }}">
                            </div>
                            <div class="col-md-7">
                                {{-- <p class="notes anek-telugu">Notes</p> --}}
                                <p class="introducin1">Dr. Nicola Z Gore</p>
                                <p class="step_into1_">BDS MClinDent (Fixed & Removable Prosthodontics)
                                    MJDF RCS
                                    PG Cert (Dental Education)
                                    PG Dip Orthodontics
                                    Fellowship (College Of General Dentist) FCGDent</p>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="box">

                        <div class="row">
                            <div class="col-md-5">
                                <img class="" src="{{ asset('images/assist/profile2.png') }}">
                            </div>
                            <div class="col-md-7">
                                {{-- <p class="notes anek-telugu">Notes</p> --}}
                                <p class="introducin1">Dr. Raabiha Maan</p>
                                <p class="step_into1_">BDS (Hons)
                                    Postgraduate Diploma in Restorative and Aesthetic Dentistry
                                    Dr Raabiha graduated with BDS honours from Barts and the London School of Medicine and
                                    Dentistry in 2014.</p>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


        </div>
    </div>
@endsection
