@extends ('layouts.layout_2')

@section('head')
    <title>Home &#8211; Dian</title>
@endsection

<style>
   .box {
    background-color: #102335;
    /* width: 300px; */
    /* border: 15px solid green; */
    padding: 35px;
    margin-bottom: 20px;
    height: 260px;
}
    .w-6 {
        width: 6rem !important;
    }

    .notes {
        font-size: 30px;
        font-weight: 300;
    }

    .speech_to_ {
        font-size: 32px;
        font-weight: 400;
        color: rgba(255, 255, 255, 1);
    }
</style>



@section('content')
    <div class="content-body">

        {{-- @include('pages.subheader') --}}


        <div class="container-fluid">

            <p class="please_acc anek-telugu">Please access the ASSIST feature only in laptops and desktop computers. We are currently working towards
                making in available in other form factors like Tablets and mobile and will be available soon!</p>
            <div class="row">

                <div class="col-md-6">

                    <div class="box">

                        <div class="row">
                            <img class="w-6" src="{{ asset('images/assist/speech-to-text.png') }}">
                        </div>

                        <br>
                        <div class="row">
                            <p class="notes anek-telugu">Notes</p>


                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="speech_to_">Speech To Text</p>
                            </div>
                            <div class="col-md-2">
                                <a href="{{ route('speech-to-text') }}">   <img class="" src="{{ asset('images/assist/next.png') }}"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="box">

                        <div class="row">
                            <img class="w-6" src="{{ asset('images/assist/email.png') }}">
                        </div>

                        <br>
                        <div class="row">
                            <p class="notes anek-telugu">Patient</p>


                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="speech_to_">Email Sender</p>
                            </div>
                            <div class="col-md-2">
                                <a href="{{ route('emailsender') }}">  <img class="" src="{{ asset('images/assist/next.png') }}"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="py-2">
                <div class="row">

                    <div class="col-md-6">

                        <div class="box">

                            <div class="row">
                               <img class="w-6" src="{{ asset('images/assist/mouse.png') }}">
                            </div>

                            <br>
                            <div class="row">
                                <p class="notes anek-telugu">Notes</p>


                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <p class="speech_to_">Templates</p>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('templates') }}"> <img class="" src="{{ asset('images/assist/next.png') }}"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="box">

                            <div class="row">
                                <img class="w-6" src="{{ asset('images/assist/explainer.png') }}">
                            </div>

                            <br>
                            <div class="row">
                                <p class="notes anek-telugu">Patient</p>


                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <p class="speech_to_">Explainer Videos</p>
                                </div>
                                <div class="col-md-2">
                                    <img class="" src="{{ asset('images/assist/next.png') }}">
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn1 btn-secondary anek-telugu">New content released every
                            month</button>
                    </div>
                </div>

            </div>



        </div>
    </div>
@endsection
