<?php

use App\Http\Controllers\GoogleAuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/pubmed-search',function(){
    return view('pubmed');
});

Route::get('/subscription-plans',[StripeController::class, 'subscriptionPlans'])->name('subscriptionPlans');

Route::post('/submit-subscription-plan',[StripeController::class,'submitSubscriptionPlans'])->name('submitSubscriptionPlans');


Route::get('/upgrade-subscription',[StripeController::class,'upgradeSubscription'])->name('upgrade-subscription');

Route::get('stripe', [StripeController::class, 'stripe'])->name('stripe');

Route::get('stripe', [StripeController::class, 'stripePost'])->name('stripe.post');

Route::post('/cancel-subscription', [App\Http\Controllers\StripeController::class, 'cancelSubscription'])->name('cancelSubscription');

Route::get('/complete-checkout',[App\Http\Controllers\StripeController::class,'completeCheckout'])->name('complete-checkout');

Route::get('/complete-upgrade-checkout',[App\Http\Controllers\StripeController::class,'completeUpgradeCheckout'])->name('complete-upgrade-checkout');

// Route::get('/', function () {
//     return view('welcome');
//     return view('pages.home');
// });

// Route::get('/', function () {
//     // return view('welcome');
//     return view('pages.assist');
// });

Route::get('/subscribe', function () {
    $response = 'Welcome to Dian...';

    return view('subscribe', ['response' => $response]);
});

Route::post('/save-video', [App\Http\Controllers\VideoRecorderController::class, 'saveVideo'])->name('save-video');

Route::get('/shopify-logo-url', [App\Http\Controllers\DashboardController::class, 'shopifyLogoUrl'])->name('shopifyLogoUrl');

Route::post('/submit-subscribe', [App\Http\Controllers\DashboardController::class, 'submitSubscribe'])->name('submitSubscribe');

Route::get('/pubMed', [App\Http\Controllers\DashboardController::class, 'pubMed'])->name('pubMed');

Route::post('/submit-pubMed', [App\Http\Controllers\DashboardController::class, 'submitPubMed'])->name('submitPubMed');

Route::get('/copd', [App\Http\Controllers\DashboardController::class, 'copd'])->name('copd');

Route::post('/submitCopd',[App\Http\Controllers\DashboardController::class, 'submitcopd'])->name('submitcopd');

Route::get('/signin', [App\Http\Controllers\GoogleAuthController::class, 'signin'])->name('signin');

Route::get('/signin2', function () {
    return view('signin2');
})->name('signin2');

Route::get('/test-vimeo', function () {
    return view('testVimeo');
})->name('testVimeo');

Route::get('auth/google', [App\Http\Controllers\GoogleAuthController::class, 'redirect'])->name('google-auth');

Route::get('auth/google2', [App\Http\Controllers\GoogleAuthController::class, 'redirect2'])->name('google-auth2');

Route::get('auth/google/call-back', [App\Http\Controllers\GoogleAuthController::class, 'callBackGoogle']);

Route::get('auth/google/call-back2', [App\Http\Controllers\GoogleAuthController::class, 'callBackGoogle2']);

Route::get('/auth/apple/login', [App\Http\Controllers\GoogleAuthController::class, 'redirectToApple'])->name('apple-auth');;

Route::post('/auth/apple/call-back', [App\Http\Controllers\GoogleAuthController::class, 'handleAppleCallback']);

Route::get('/logout', [App\Http\Controllers\GoogleAuthController::class, 'logout'])->name('logout');

Route::get('/home', [App\Http\Controllers\DashboardController::class, 'home'])->name('home');

Route::get('/welcome', [App\Http\Controllers\DashboardController::class, 'home'])->name('welcome');
Route::get('/', [App\Http\Controllers\DashboardController::class, 'home']);

Route::get('/privacy-policy', [App\Http\Controllers\DashboardController::class, 'privacyPolicy'])->name('privacyPolicy');

Route::get('/terms-and-conditions', [App\Http\Controllers\DashboardController::class, 'termsAndConditions'])->name('termsAndConditions');

Route::get(
    '/dashboard',
    // [App\Http\Controllers\DashboardController::class, 'dashboard']
    function(){
        return view('pages.home');
    }
)->name('dashboard');

// Route::get(
//     '/redirecttonotes/{template',
//     [App\Http\Controllers\DashboardController::class, 'dashboardRedirectNotes']
// );

// Route::get(
//     '/welcome',
//     [App\Http\Controllers\DashboardController::class, 'welcome']
// )->name('welcome');

Route::get('/hashtag-filter', [\App\Http\Controllers\DashboardController::class, 'hashtagFilter'])->name('hashtag-filter');

Route::get('/business-finance-hashtag-filter',[\App\Http\Controllers\DashboardController::class, 'businessFinanceHashtagFilter'])->name('business-finance-hashtag-filter');

Route::get('/hashtag-filter2', [\App\Http\Controllers\DashboardController::class, 'hashtagFilter2'])->name('hashtag-filter2');

Route::get('/setup-profile',[App\Http\Controllers\DashboardController::class, 'setupProfile'])->name('setup-profile');

Route::post('/upload-initial-profile-data',[App\Http\Controllers\DashboardController::class, 'uploadInitialProfileData'])->name('upload-initial-profile-data');

Route::get('/profile',[App\Http\Controllers\DashboardController::class, 'profile'])->name('profile');

Route::post('/update-initial-profile-data', [App\Http\Controllers\DashboardController::class, 'uploadInitialProfileData'])->name('update-initial-profile-data');

Route::post('/update-user-profile', [App\Http\Controllers\DashboardController::class, 'updateUserProfile'])->name('update-user-profile');

Route::get('/podcast', [App\Http\Controllers\DashboardController::class, 'podcast'])->name('podcast');

// Route::get('/assist', [App\Http\Controllers\DashboardController::class, 'assist'])->name('assist');
Route::get('/assist', [App\Http\Controllers\AssistController::class, 'assist'])->name('assist');
Route::get('/speech-to-text', [App\Http\Controllers\SpeechtotextController::class, 'speechtotext'])->name('speech-to-text');
Route::get('/pricing',[App\Http\Controllers\PricingController::class,'pricing'])->name('pricing');
Route::get('/emailsender', [App\Http\Controllers\EmailsenderController::class, 'emailsender'])->name('emailsender');
Route::get('/templates', [App\Http\Controllers\TemplatesController::class, 'templates'])->name('templates');
Route::get('/explainervideos', [App\Http\Controllers\ExplainervideosController::class, 'explainervideos'])->name('explainervideos');

Route::get('/courses', [App\Http\Controllers\DashboardController::class, 'courses'])->name('courses');

Route::get('/courses1', [App\Http\Controllers\DashboardController::class, 'courses1'])->name('courses1');

Route::get('/health-and-wellbeing', [App\Http\Controllers\DashboardController::class, 'healthAndWellbeing'])->name('healthAndWellbeing');

Route::get('/build-your-business', [App\Http\Controllers\DashboardController::class, 'buildYourBusiness'])->name('buildYourBusiness');

Route::get('/downloads1', [App\Http\Controllers\DashboardController::class, 'downloads'])->name('downloads1');

Route::get('/student1', [App\Http\Controllers\DashboardController::class, 'student'])->name('student1');

Route::get('/forums', [App\Http\Controllers\ForumController::class, 'forums'])->name('forums');

Route::get('/blogs', [App\Http\Controllers\DashboardController::class, 'blogs'])->name('blogs'); // not using

Route::get('/all-blogs', [App\Http\Controllers\DashboardController::class, 'allBlogs'])->name('allBlogs');

Route::get('/work-flows', [App\Http\Controllers\DashboardController::class, 'workFlows'])->name('workFlows');

Route::get('/guidelines1', [App\Http\Controllers\DashboardController::class, 'guidelines'])->name('guidelines1');

Route::post('/single-blog', [App\Http\Controllers\DashboardController::class, 'singleBlog'])->name('single-blog');

Route::get('/refer', [App\Http\Controllers\DashboardController::class, 'refer'])->name('refer');

Route::get('/guidelines', [App\Http\Controllers\DashboardController::class, 'guidelines'])->name('guidelines');

Route::get('/index', [App\Http\Controllers\ForumController::class, 'index'])->name('index');

Route::get('/threads', [App\Http\Controllers\ForumController::class, 'threads'])->name('threads');

Route::get('/ask-question', [App\Http\Controllers\ForumController::class, 'askQuestion'])->name('ask-question');

Route::post('/submit-question', [App\Http\Controllers\ForumController::class, 'submitQuestion'])->name('submit-question');

Route::get('/single-category/{questionId}', [App\Http\Controllers\ForumController::class, 'singleCategory'])->name('single-category');

Route::get('/delete-forum-question/{questionId}',[App\Http\Controllers\ForumController::class,'deleteForumQuestion'])->name('delete-forum-question');

Route::post('/submit-thread', [App\Http\Controllers\ForumController::class, 'submitThread'])->name('submit-thread');

Route::get('/shopify-home', [App\Http\Controllers\ShopifyController::class, 'shopifyHome'])->name('shopify-home');

Route::get('/about-us', [App\Http\Controllers\AboutUsController::class, 'aboutUs'])->name('aboutUs');

// Route::get('/faq', [App\Http\Controllers\DashboardController::class, 'faq'])->name('faq');
Route::get('/faq', [App\Http\Controllers\FaqController::class, 'faq'])->name('faq');

// Route::get('/contact', [App\Http\Controllers\DashboardController::class, 'contact'])->name('contact');
Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contact'])->name('contact');

Route::get('/packageInfo', [App\Http\Controllers\DashboardController::class, 'packageInfo'])->name('packageInfo');

Route::get('/page1',[App\Http\Controllers\TemplateController::class,'page1'])->name('page1');

Route::get('/email-template',[App\Http\Controllers\TemplateController::class,'emailTemplate'])->name('emailTemplate');

Route::get('/speech-to-text-notes',[App\Http\Controllers\TemplateController::class,'speechToTextNotes'])->name('speechToTextNotes');

// Route::get('/all-templates',[App\Http\Controllers\TemplateController::class,'allTemplates'])->name('allTemplates');

Route::post('/template-notes',[App\Http\Controllers\TemplateController::class,'templateNotes'])->name('templateNotes');

Route::get('/patient-notes/{templateId}',[App\Http\Controllers\TemplateController::class,'patientNotes'])->name('patientNotes');

Route::post('/save-patient-notes',[App\Http\Controllers\TemplateController::class,'savePatientNotes'])->name('savePatientNotes');

Route::post('/save-speech-to-text',[App\Http\Controllers\TemplateController::class,'saveSpeechToText'])->name('saveSpeechToText');

Route::get('/saved-patient-notes/{templateId}',[App\Http\Controllers\TemplateController::class,'displaySavedPatientNotes'])->name('displaySavedPatientNotes');

Route::get('/delete-patientnotes-24hours',[App\Http\Controllers\TemplateController::class,'deletePatientNotes'])->name('deletePatientNotes'); // RUNS DAILY

Route::get('/assist-videos',[App\Http\Controllers\DashboardController::class,'assistVideos'])->name('assistVideos');

Route::post('/send-patient-email',[App\Http\Controllers\TemplateController::class,'sendPatientEmail'])->name('sendPatientEmail');

Route::get('/check-renewel-subscription-notifications',[App\Http\Controllers\StripeController::class,'checkRenewlSubscriptionNotifications'])->name('checkRenewlSubscriptionNotifications');// RUNS DAILY

Route::get('/admin-login',[App\Http\Controllers\AdminController::class,'adminLogin'])->name('admin-login');

Route::post('/check-admin-login',[App\Http\Controllers\AdminController::class,'checkAdminLogin'])->name('checkAdminLogin');

Route::post('/add-discount-coupon',[App\Http\Controllers\AdminController::class,'addDiscountCoupon'])->name('addDiscountCoupon');

Route::post('/update-subscription-plan',[App\Http\Controllers\AdminController::class,'updateSubscriptionPlan'])->name('updateSubscriptionPlan');

// Route::get('/speech-to-text',[App\Http\Controllers\GoogleCloudSpeechController::class,'speechToText'])->name('speechToText');

Route::get('/voice-recorder-test',[App\Http\Controllers\GoogleCloudSpeechController::class,'voiceRecorder'])->name('voiceRecorder');

// routes/web.php
Route::post('/save-audio-test', [App\Http\Controllers\GoogleCloudSpeechController::class,'saveAudio'])->name('saveAudio');
