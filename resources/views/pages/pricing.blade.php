@extends ('layouts.layout_2')

@section('head')
    <title>Home &#8211; Dian</title>
@endsection

<style>


</style>



@section('content')
    <div class="content-body">

        {{-- @include('pages.subheader') --}}

        <div class="container">
            <p class="introducin text-center">Join The Dian Club</p>
            <p class="speech_to_ text-center">Select your plan</p>

            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="pricing-container ">
                        <div class="toggle-buttons text-center pb-sm-5">
                            <button id="monthly-btn" class="active btn2  anek-telugu" onclick="togglePlan('monthly')">Monthly</button>
                            <button id="yearly-btn" class="btn4  anek-telugu" onclick="togglePlan('yearly')">Yearly</button>
                        </div>

                        {{-- <div class=" pt-0">

                            <button type="button" class="btn2  anek-telugu mr-13">Save</button>

                            <button type="button" class="btn3  anek-telugu">Copy text</button>

                        </div> --}}

                        <div class="pricing-plans" id="monthly-plans">
                            <!-- Monthly Plans Content -->
                            <div class="row">
                                <div class="col-md-3">
                                    <img class="w-17" src="{{ asset('images/assist/starter.png') }}">

                                        <div class="vertical-menu">
                                            <div class="height">
                                                <DIV class="videos2">
                                            <p class="border-bottom">Videos (2 free videos)</p>
                                            <p class="border-bottom">Podcasts only</p>
                                            <p class="border-bottom">Blogs</p>
                                            <p >Forum (View only)</p>

                                        </DIV>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <img class="w-17" src="{{ asset('images/assist/12.png') }}">

                                        <div class="vertical-menu">
                                            <div class="height">
                                                <DIV class="videos2">
                                            <p class="border-bottom">Videos (2 free videos)</p>
                                            <p class="border-bottom">Podcasts only</p>
                                            <p class="border-bottom">Student</p>
                                            <p class="border-bottom">Workflows</p>
                                            <p>Forum</p>

                                        </DIV>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <img class="w-17" src="{{ asset('images/assist/premium.png') }}">

                                        <div class="vertical-menu">
                                            <div class="height">
                                                <DIV class="videos2">
                                            <p class="border-bottom">Videos</p>
                                            <p class="border-bottom">Podcasts and Webinars</p>
                                            <p class="border-bottom">Assist</p>
                                            <p class="border-bottom">Blogs</p>
                                            <p class="border-bottom">Student</p>
                                            <p class="border-bottom">Business and Finance</p>
                                            <p class="border-bottom">Downloads</p>
                                            <p class="border-bottom">Wellbieng</p>
                                            <p class="border-bottom">Courses</p>
                                            <p class="border-bottom">Guidelines</p>
                                            <p class="border-bottom">Workflows</p>
                                            <p >Forum</p>

                                        </DIV>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <img class="w-17" src="{{ asset('images/assist/24.png') }}">

                                        <div class="vertical-menu">
                                            <div class="height">
                                                <DIV class="videos2">
                                            <p class="border-bottom">Videos</p>
                                            <p class="border-bottom">Podcasts and Webinars</p>
                                            <p class="border-bottom">Assist</p>
                                            <p class="border-bottom">Blogs</p>
                                            <p class="border-bottom">Student</p>
                                            <p class="border-bottom">Business and Finance</p>
                                            <p class="border-bottom">Downloads</p>
                                            <p class="border-bottom">Wellbieng</p>
                                            <p class="border-bottom">Courses</p>
                                            <p class="border-bottom">Guidelines</p>
                                            <p class="border-bottom">Workflows</p>
                                            <p >Forum</p>

                                        </DIV>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pricing-plans" id="yearly-plans" style="display: none;">
                            <!-- Yearly Plans Content -->
                            <div class="plan">
                                <h2>Basic Yearly</h2>
                                <p>$100/year</p>
                            </div>
                            <div class="plan">
                                <h2>Pro Yearly</h2>
                                <p>$200/year</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

<script>
    function togglePlan(planType) {
        const monthlyBtn = document.getElementById('monthly-btn');
        const yearlyBtn = document.getElementById('yearly-btn');
        const monthlyPlans = document.getElementById('monthly-plans');
        const yearlyPlans = document.getElementById('yearly-plans');

        if (planType === 'monthly') {
            monthlyBtn.classList.add('active');
            yearlyBtn.classList.remove('active');
            monthlyPlans.style.display = 'contents';
            yearlyPlans.style.display = 'none';
        } else {
            yearlyBtn.classList.add('active');
            monthlyBtn.classList.remove('active');
            yearlyPlans.style.display = 'flex';
            monthlyPlans.style.display = 'none';
        }
    }
</script>
