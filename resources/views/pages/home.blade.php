@extends ('layouts.layout_2')

@section('head')
    <title>Home &#8211; Dian</title>
@endsection




@section('content')
    <div class="content-body">

        @include('pages.subheader')

        <style>
            .videoContainer {
              position: relative;
            }

            /* .w-28 {
    max-height: 231px;
    width: 28rem;
    position: relative;
    top: 2rem;
} */

            .playButton {
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%, -50%);
              background: none;
              border: none;
              font-size: 48px; /* Adjust the font size as needed */
              color: #fff; /* Adjust the color as needed */
              cursor: pointer;
            }
          </style>

        <div class="container-fluid ">
            <div class="row">

                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/video1.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/video/845503149?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479">
                            <script src="https://player.vimeo.com/api/player.js"></script>


                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>



                    {{-- <div id="activeMenu" value="dashboard"></div>
                    @if(session('finalReels'))
                    @foreach($finalReels as $finalReel)

                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/video1.png') }}" alt="Video Thumbnail">
                        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src={{ $finalReel->url }} frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Test"></iframe></div>
                        <script src="https://player.vimeo.com/api/player.js"></script>
                        <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        <!-- <h1 class="dashboard-name">{{ $finalReel->name }}</h1> -->
                    </div>
                    @endforeach
                    @endif --}}


                </div>

                {{-- <div class="col-md-4">
                    <div class="col-md-4 col-sm-12 col-xs-12" style="margin-bottom:20px;">
                        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/845755194?app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Test" data-ready="true"></iframe></div>
                        <script src="https://player.vimeo.com/api/player.js"></script>
                        <!-- <h1 class="dashboard-name">Podcast 4 - BDS &amp; Beyond</h1> -->
                    </div>
                </div> --}}


                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/teeth.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/cc4ea410-b752-4c44-9faf-4aa6b12c3ba4?autoplay=1&loop=1&autopause=0">


                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/3.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">


                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>






            </div>
            <div class="row py-3">

                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/4.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/5.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/6.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
            </div>
            <div class="row py-4">

                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/7.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/8.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="videoContainer">
                        <img class="w-28" src="{{ asset('images/dashboard/9.png') }}" alt="Video Thumbnail">
                        <button class="playButton"
                            data-src="https://player.vimeo.com/fa22c2d6-33bb-4d8a-8823-3b4599c9b267">

                            <img class="" src="{{ asset('images/dashboard/videoicon.png') }}" alt="Video Thumbnail">
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
          $(".playButton").on("click", function() {
            var videoURL = $(this).data("src");

            // Create the iframe with the YouTube video URL
            var iframe = $('<iframe>', {
              width: "100%",
              height: "290",
              src: videoURL,
              title: "YouTube video player",
              frameborder: 0,
              allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share",
              allowfullscreen: true
            });

            // Replace the image and button with the iframe
            $(this).siblings('img').hide();
            $(this).replaceWith(iframe);
          });
        });
        </script>

@endsection
