@extends ('layouts.layout_2')

@section('head')
    <title>Home &#8211; Dian</title>
@endsection

<style>
    .videoContainer {
        position: relative;
    }

    .playButton {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background: none;
        border: none;
        font-size: 48px;
        /* Adjust the font size as needed */
        color: #fff;
        /* Adjust the color as needed */
        cursor: pointer;
    }
</style>


@section('content')
    <div class="content-body">

        <div class="container-fluid">
            <div class="row">

                <p class="introducin2">Saved Notes</p>

                <p class="step_into_">No saved notes
                </p>
                <p class="introducin2">Select Template</p>
                <div class="col-md-12">
                    <div class="bootstrap-badge">
                        <span class="badge badge-primary">Comprehensive Exam</span>
                        <span class="badge badge-primary">Routine Exam</span>
                        <span class="badge badge-primary">Filling Composite</span>
                        <span class="badge badge-primary">Child Exam</span>
                        <span class="badge badge-primary">Treatment Options</span>

                        <br />
                        <br />

                        <span class="badge badge-primary">Treatment Options</span>
                        <span class="badge badge-primary">Filling: amalgam or GIC</span>
                        <span class="badge badge-primary">Emergency Appointment</span>
                        <span class="badge badge-primary">RCT 1</span>
                        <span class="badge badge-primary">RCT 2</span>

                        <br />
                        <br />

                        <span class="badge badge-primary">Dry Socket</span>
                        <span class="badge badge-primary">Pericoronitis</span>
                        <span class="badge badge-primary">Crown Preparation</span>
                        <span class="badge badge-primary">Crown / bridge fit</span>

                    </div>
                </div>
            </div>

            <div class="row py-4">
                <div class="col-md-12">
                    <button type="button" class="btn1 btn-secondary anek-telugu">New content released every
                        month</button>
                </div>
            </div>
        </div>

    </div>
@endsection
